/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.model;

import java.sql.ResultSet;

/**
 *
 * @author Chanon
 */
public class User {

    public static User fromRS(ResultSet rs) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    private int id;
    private String name;
    private String password;
    private String gender;
    private int role;

    public User(int id, String name, String password, String gender, int role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public User(String name, String password, String gender, int role) {
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public User() {
        this.id = -1;
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }
    
 
}
